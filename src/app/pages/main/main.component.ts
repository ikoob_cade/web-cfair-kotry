import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, HostListener } from '@angular/core';
import { SpeakerService } from 'src/app/services/api/speaker.service';
import { BoothService } from 'src/app/services/api/booth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;
  public excerptUrl = 'https://cfair-files.s3.ap-northeast-2.amazonaws.com/events/5f91270e0a95be0011d250b4/2020+KOTRY+SYMPOSIUM+%E1%84%8E%E1%85%A9%E1%84%85%E1%85%A9%E1%86%A8%E1%84%8C%E1%85%B5%E1%86%B8.pdf'; // TODO KOTRY2020 초록집 URL 기입

  public attachments = []; // 부스 첨부파일

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.loadSpeakers();
    this.loadBooths();
  }

  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) { limit = 6; }
    this.speakerService.find(true).subscribe(res => {
      let speakers = [];
      _.forEach(res['Part i'], speaker => {
        speakers.push(speaker);
      });

      _.forEach(res['Part ii'], speaker => {
        speakers.push(speaker);
      });

      this.speakers = this.division(speakers, limit);
    });
  }

  /**
   * 스폰서 목록 조회
   */
  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      this.sponsors = res;
    });
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  // 부스 상세보기
  getDetail = (selectedBooth) => {
    this.boothService.findOne(selectedBooth.id).subscribe(res => {
      this.selectedBooth = res;
      this.setAttachments();
      this.setDesc();
    });
  }

}
