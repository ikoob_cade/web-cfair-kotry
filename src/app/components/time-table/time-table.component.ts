import { Component, OnInit, Input, HostListener, ViewChild, ElementRef, ChangeDetectorRef, AfterContentChecked, Output, EventEmitter } from '@angular/core';
import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { DateService } from 'src/app/services/api/date.service';
import { RoomService } from 'src/app/services/api/room.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit, AfterContentChecked {

  public dates: any;
  public rooms: any;

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private dateService: DateService,
    private roomService: RoomService
  ) { }

  ngOnInit(): void {
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .subscribe(resp => {
        this.dates = resp[0];
        this.rooms = resp[1];
      });
  }

  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
  }



  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * Live 상세보기
   * @param date 순서
   * @param room 순서
   */
  goLive(date: number, room: number): void {
    this.router.navigate(['/live'], { queryParams: { dateId: this.dates[date].id, roomId: this.rooms[room].id } });
  }

  public scrollToTable(): void {
  }

}
